package core;

import game.Ship;
import game.util.Position;

import graphic.Board;

import java.util.ArrayList;
import java.util.Scanner;
import game.util.SaveGame;

/**
 *
 * O objetivo desta classe é gerenciar os ataques feitos
 * 
 * @author rosemberg
 */
public class BattleSystem {
    private ArrayList<String> messages = new ArrayList<>();
    private ArrayList<Position> attackedPositions = new ArrayList<>();
    private SaveGame savegame = new SaveGame();
    
    /**
     * 
     * Método que inicializa as funcionalidades de combate do jogo
     * 
     * @param board Tabuleiro que será utilizado na batalha
     * @param ships Array de navios inclusos no jogo
     */    
    public void screen(Board board, ArrayList<Ship> ships) {
        Scanner input = new Scanner(System.in);
        String keys = "";
        Integer line = 0;
        Integer column = 0;
       savegame.loadGameByXML(board);
       
       while (!this.isEndBattle(ships)) {
                 
            board.showBoard();
            //board.showHideShips();
            
            this.showMessages();
                        
            System.out.println("=>Shoot");
            System.out.print("	Line   : ");
            keys = input.nextLine();
            line = (Integer.parseInt(keys.substring(0, 2)) - 1);
            System.out.print("	Column : ");
            keys = input.nextLine();
            column = this.convertIntegerColumn(keys.substring(0, 1));

            this.attack(board, ships, line, column);
            
            savegame.saveGameInXML(board);
            savegame.saveGameInJSON(board);
        }
    }

    private void attack(Board board, ArrayList<Ship> ships, Integer line, Integer column) {
        Position position = null;
        
        for(int i = 0; i < this.attackedPositions.size(); ++i) {
            position = this.attackedPositions.get(i);
            
            if(position.compareTo(new Position(line, column)) == 0) {
                return;
            }
        }
        
        this.attackedPositions.add(new Position(line, column));
        
        if (board.isEmpty(line, column) || board.grid[line][column] == 'O') {
            board.setPosition('O', line, column);
        } else {
            board.setPosition('X', line, column);
            this.setDamageShip(ships, line, column);
        }
    }

    private void setDamageShip(ArrayList<Ship> ships, Integer line, Integer column) {
        for (int i = 0; i < ships.size(); ++i) {
            Ship ship = ships.get(i);

            if (ship.isReached(line, column)) {
                ship.increaseDamage(1);

                if (ship.isDestroyed()) {
                    ships.remove(i);
                    this.generateMessage(ship);
                }
                
                break;
            }
        }
    }

    private boolean isEndBattle(ArrayList<Ship> ships) {
        return (ships.size() <= 0);
    }

    private Integer convertIntegerColumn(String letter) {
        return (Integer.valueOf(letter.toUpperCase().charAt(0) - 'A'));
    }
    
    private void generateMessage(Ship ship) {
        this.messages.add("The ship " + ship.getFullname() + " was destroyed");
    }
    
    private void showMessages() {
        for(int i = 0; i < this.messages.size(); ++i) {
            System.out.println(this.messages.get(i));
        }
        
        System.out.println();
    }
    
}
