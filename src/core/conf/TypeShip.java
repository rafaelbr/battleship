package core.conf;

/**
 * O objetivo desta enumeração é definir as quantidade de navios
 * 
 * @author Rosemberg Rodrigues
 */
public enum TypeShip {

    /**
     * Constante que representa a quantidade por categoria de navio
     */
    AIRCRAFT(1), BATTLESHIP(1), SUBMARINE(2), DESTROYER(3), PATROL(4);

    private final Integer quantity;

    private TypeShip(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * Método para recuperar a quantidade por navio
     * 
     * @return Integer - Quantidade por navio
     */
    public Integer getQuantity() {
        return this.quantity;
    }
}
