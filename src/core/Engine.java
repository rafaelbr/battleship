package core;

import core.conf.TypeShip;
import game.Aircraft;
import game.Battleship;
import game.Destroyer;

import game.Patrol;
import game.Ship;
import game.Submarine;

import graphic.Board;
import graphic.conf.Dimension;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * O objetivo desta classe é inicializar as partes que compõe o jogo (tabuleiro, batalha, navios)
 * 
 * @author rosemberg
 */
public class Engine {

    private Board board = new Board();
    private ArrayList<Ship> ships = new ArrayList<>();
    private BattleSystem battleSystem = new BattleSystem();

    /**
     * 
     * Método que inicializa o jogo
     * 
     */
    public void initGame() {
        this.generateShips();
        this.allocateShips();
        this.loadGame();
    }

    private void generateShips() {
        for (int i = 0; i < TypeShip.AIRCRAFT.getQuantity(); ++i) {
            this.ships.add(new Aircraft());
        }

        for (int i = 0; i < TypeShip.BATTLESHIP.getQuantity(); ++i) {
            this.ships.add(new Battleship());
        }

        for (int i = 0; i < TypeShip.SUBMARINE.getQuantity(); ++i) {
            this.ships.add(new Submarine());
        }

        for (int i = 0; i < TypeShip.DESTROYER.getQuantity(); ++i) {
            this.ships.add(new Destroyer());
        }

        for (int i = 0; i < TypeShip.PATROL.getQuantity(); ++i) {
            this.ships.add(new Patrol());
        }
    }

    private void allocateShips() {
        Random random = new Random();
        Integer line = -1;
        Integer column = -1;
        Integer direction = -1;
        Ship ship = null;

        for (int i = 0; i < this.ships.size(); ++i) {
            line = random.nextInt(Dimension.SIZE.getLine());
            column = random.nextInt(Dimension.SIZE.getColumn());
            direction = random.nextInt(2);

            ship = this.ships.get(i);
            ship.setDirection(direction);

            while (!this.board.setShip(ship, line, column)) {
                line = random.nextInt(Dimension.SIZE.getLine());
                column = random.nextInt(Dimension.SIZE.getColumn());
            }
        }
    }

    private void loadGame() {
        this.battleSystem.screen(board, ships);
    }
}
