package graphic;

import game.Ship;

import graphic.conf.Dimension;

/**
 * O objetivo desta classe é manipular as operações feitas no tabuleiro
 * 
 * @author rosemberg
 */
public class Board {

    public Dimension dimension = Dimension.SIZE;
    public char[][] grid = new char[this.dimension.getLine()][this.dimension.getColumn()];

    /**
     * Construtor de classe que inicializa as posições do tabuleiro
     */
    public Board() {
        for (int i = 0; i < this.dimension.getLine(); ++i) {
            for (int j = 0; j < this.dimension.getColumn(); ++j) {
                this.grid[i][j] = '\0';
            }
        }
    }
    
    /**
     * 
     * Método para incluir um navio no tabuleiro
     * 
     * @param ship Navio a ser inserido
     * @param line Linha na qual se pretende inserir o navio
     * @param column Coluna na qual se pretende inserir o navio
     * @return boolean - Retorna "true" caso tenha conseguido inserir o navio no tabuleiro 
     */
    public boolean setShip(Ship ship, Integer line, Integer column) {
        Integer horizontalDirection = -1;
        Integer verticalDirection = -1;

        if (ship.getDirection() == 0) {
            ship.transpose();
        }

        if (ship.getHorizontalSize() > 0) {
            horizontalDirection = this.isEmptyHorizontalDirection(line, column, ship.getHorizontalSize());

            if (horizontalDirection != 0) {
                this.setHorizontalPosition(ship, line, column, horizontalDirection);
            } else {
                return false;
            }
        } else if (ship.getVerticalSize() > 0) {
            verticalDirection = this.isEmptyVerticalDirection(line, column, ship.getVerticalSize());

            if (verticalDirection != 0) {
                this.setVerticalPosition(ship, line, column, verticalDirection);
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * 
     * Método para marcar a letra que representa um navio no tabuleiro
     * 
     * @param name Letra que representa o navio
     * @param line Linha na qual se pretende marcar a letra
     * @param column Coluna na qual se pretende marcar a letra
     */
    public void setPosition(char name, Integer line, Integer column) {
        this.grid[line][column] = name;
    }

    /**
     * 
     * Método para verificar se uma determinada posição está vazia
     * 
     * @param line Linha na qual se deseja verificar
     * @param column Coluna na qual se deseja verificar
     * @return boolean - Retorna "true" caso a posição esteja vazia
     */
    
    public boolean isEmpty(Integer line, Integer column) {
        try {
            return (this.grid[line][column] == '\0');
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            return false;
        }
    }

    private boolean isEmptyVertical(Integer init, Integer finish, Integer column) {
        boolean empty = true;

        for (int i = init; i <= finish; ++i) {
            empty = empty && this.isEmpty(i, column);
        }

        return empty;
    }

    private boolean isEmptyUpSide(Integer line, Integer column, Integer verticalSize) {
        return this.isEmptyVertical((line - verticalSize), line, column);
    }

    private boolean isEmptyDownSide(Integer line, Integer column, Integer verticalSize) {
        return this.isEmptyVertical(line, (line + verticalSize), column);
    }

    private Integer isEmptyVerticalDirection(Integer line, Integer column, Integer verticalSize) {
        if (this.isEmptyUpSide(line, column, verticalSize)) {
            return -1;
        } else if (this.isEmptyDownSide(line, column, verticalSize)) {
            return 1;
        }

        return 0;
    }

    private void setVerticalPosition(Ship ship, Integer line, Integer column, Integer direction) {
        if (direction < 0) {
            this.setUpPosition(ship, line, column);
        } else {
            this.setDownPosition(ship, line, column);
        }
    }

    private void setUpPosition(Ship ship, Integer line, Integer column) {
        this.setVertical((line - ship.getVerticalSize() + 1), line, column, ship);
    }

    private void setDownPosition(Ship ship, Integer line, Integer column) {
        this.setVertical(line, (line + ship.getVerticalSize() - 1), column, ship);
    }

    private void setVertical(Integer init, Integer finish, Integer column, Ship ship) {
        for (int i = init; i <= finish; ++i) {
            this.grid[i][column] = ship.getName();
            ship.setPosition(i, column);
        }
    }

    private boolean isEmptyHorizontal(Integer init, Integer finish, Integer line) {
        boolean empty = true;

        for (int j = init; j <= finish; ++j) {
            empty = empty && this.isEmpty(line, j);
        }

        return empty;
    }

    private boolean isEmptyLeftSide(Integer line, Integer column, Integer horizontalSize) {
        return this.isEmptyHorizontal((column - horizontalSize), column, line);
    }

    private boolean isEmptyRightSide(Integer line, Integer column, Integer horizontalSize) {
        return this.isEmptyHorizontal(column, (column + horizontalSize), line);
    }

    private Integer isEmptyHorizontalDirection(Integer line, Integer column, Integer horizontalSize) {
        if (this.isEmptyLeftSide(line, column, horizontalSize)) {
            return -1;
        } else if (this.isEmptyRightSide(line, column, horizontalSize)) {
            return 1;
        }

        return 0;
    }

    private void setHorizontalPosition(Ship ship, Integer line, Integer column, Integer direction) {
        if (direction < 0) {
            this.setLeftPosition(ship, line, column);
        } else {
            this.setRightPosition(ship, line, column);
        }
    }

    private void setLeftPosition(Ship ship, Integer line, Integer column) {
        this.setHorizontal((column - ship.getHorizontalSize() + 1), column, line, ship);
    }

    private void setRightPosition(Ship ship, Integer line, Integer column) {
        this.setHorizontal(column, (column + ship.getHorizontalSize() - 1), line, ship);
    }

    private void setHorizontal(Integer init, Integer finish, Integer line, Ship ship) {
        for (int j = init; j <= finish; ++j) {
            this.grid[line][j] = ship.getName();
            ship.setPosition(line, j);
        }
    }

    
    /**
     * Método para mostrar o tabuleiro do jogo
     */
    public void showBoard() {
        String labelColumns = "     ";
        char initalLabel = 'A';

        System.out.printf("\33[2J");

        System.out.println();

        for (int i = 0; i < this.dimension.getColumn(); ++i) {
            labelColumns += String.valueOf(initalLabel) + "   ";
            initalLabel = String.valueOf((char) (initalLabel + 1)).charAt(0);
        }

        System.out.println(labelColumns);

        for (int i = 0; i < this.dimension.getLine(); ++i) {
            System.out.print(String.format("%02d", (i + 1)) + " |");

            for (int j = 0; j < this.dimension.getColumn(); ++j) {
                if (this.isEmpty(i, j) || this.grid[i][j] != 'X' && this.grid[i][j] != 'O') {
                    System.out.print("   |");
                } else {
                    System.out.print(" " + this.grid[i][j] + " |");
                }
            }

            System.out.println();
        }

        System.out.println();
    }

    
    /**
     * Método utilizado para visualizar a posição dos navios no tabuleiro
     */
    public void showHideShips() {
        String labelColumns = "     ";
        char initalLabel = 'A';

        System.out.printf("\33[2J");

        System.out.println();

        for (int i = 0; i < this.dimension.getColumn(); ++i) {
            labelColumns += String.valueOf(initalLabel) + "   ";
            initalLabel = String.valueOf((char) (initalLabel + 1)).charAt(0);
        }

        System.out.println(labelColumns);

        for (int i = 0; i < this.dimension.getLine(); ++i) {
            System.out.print(String.format("%02d", (i + 1)) + " |");

            for (int j = 0; j < this.dimension.getColumn(); ++j) {
                if (this.isEmpty(i, j)) {
                    System.out.print("   |");
                } else {
                    System.out.print(" " + this.grid[i][j] + " |");
                }
            }

            System.out.println();
        }

        System.out.println();
    }
    public char[][] getGrid()
    {
        return grid;
    }
}
