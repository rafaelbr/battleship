package graphic.conf;

/**
 * O objetivo desta enumeração é definir as configurações do tabuleiro
 * @author Rosemberg Rodrigues
 * 
 */
public enum Dimension {

    /**
     * Constante que representa as dimensões do tabuleiro
     */
    SIZE(10,10);

    private final Integer line;
    private final Integer column;

    private Dimension(Integer line, Integer column) {
        this.line = line;
        this.column = column;
    }

    /**
     * Método para utilizado para retornar o número de linhas do tabuleiro
     * 
     * @return Integer - Quantidade de linhas
     */
    public Integer getLine() {
        return this.line;
    }
    
    /**
     * Método para utilizado para retornar o número de colunas do tabuleiro
     * 
     * @return Integer - Quantidade de colunas
     */
    public Integer getColumn() {
        return this.column;
    }
}
