package save.game;

import graphic.Board;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: rafaelbrasileiro
 * Date: 07/11/13
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
public class SaveJSON {

    public static void save(Board board) {
        char[][] grid = board.getGrid();
        File file = new File("saveBatalhaNaval.json");
        FileWriter writer;
        try {
            writer = new FileWriter(file);
            writer.write("{\n\r");
            int i = 0;
            for(char g1[] : grid){
                writer.write("\t{");
                int j = 0;
                for(char g2 : g1){
                    writer.write("\"" + g2 + "\"");
                    j++;
                    if (j < g1.length) {
                        writer.write(", ");
                    }

                }
                i++;
                if (i >= grid.length) {
                    writer.write(" }\n\r");
                }
                else {
                    writer.write(" },\n\r");
                }

            }
            writer.write("}");
            writer.flush();
            writer.close();

        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
