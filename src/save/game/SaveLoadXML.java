package save.game;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import graphic.Board;


public class SaveLoadXML {
	
	public void saveBoard(Board board)
	{
		char[][] grid = board.getGrid();
		File file = new File("saveBatalhaNaval.xml");
		FileWriter writer;
		try {
			writer = new FileWriter(file);
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\r");
			writer.write("<grid>\n\r");
			
			for(char g1[] : grid){
				writer.write("\t<line>\n\r");
				for(char g2 : g1){
					writer.write("\t\t<cell>\n\r");
					writer.write("\t\t\t"+g2+"\n\r");
					writer.write("\t\t</cell>\n\r");
				}
				writer.write("\t</line>\n\r");
			}
			writer.write("</grid>");
			writer.flush();
			writer.close();
			
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
}
