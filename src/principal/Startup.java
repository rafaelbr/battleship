package principal;

import core.Engine;

/**
 *
 * O objetivo desta classe é inicializar o jogo
 * 
 * @author rosemberg
 */
public class Startup {

    /**
     * 
     * Método que inicializa o jogo
     * 
     * @param args Argumentos passados por linha de comando
     */
    public static void main(String[] args) {
        Engine engine = new Engine();
        engine.initGame();
    }
}
