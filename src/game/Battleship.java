package game;

/**
 *
 * O objetivo desta classe é representar um navio "Battleship"
 * 
 * @author rosemberg
 */
public class Battleship extends Ship {

    /**
     * 
     * Construtor de classe que cria um Battleship
     * 
     */
    public Battleship() {
        super('B', "BattleShip", 4, 0);
    }
}
