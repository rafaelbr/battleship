package game;

/**
 *
 * O objetivo desta classe é representar um navio "Patrol"
 * 
 * @author rosemberg
 */
public class Patrol extends Ship {

    /**
     * 
     * Construtor de classe que cria um Patrol
     * 
     */
    public Patrol() {
        super('P', "Patrol", 1, 0);
    }
}
