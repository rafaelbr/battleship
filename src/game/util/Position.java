package game.util;

/**
 *
 * O objetivo desta classe é manipular as posições de linha x coluna do tabuleiro
 * 
 * @author rosemberg
 */
public class Position{

    private Integer line = -1;
    private Integer column = -1;

    /**
     * 
     * Construtor de classe que criar posição baseado em uma linha e uma coluna
     * 
     * @param line Linha da posição
     * @param column Coluna da posição
     */
    public Position(Integer line, Integer column) {
        this.line = line;
        this.column = column;
    }

    /**
     * 
     * Método utilizado para retornar a linha da posição
     * 
     * @return Integer - Linha da posição 
     */
    public Integer getLine() {
        return this.line;
    }

    /**
     * 
     * Método utilizado para incluir uma linha na posição
     * 
     * @param line Linha a ser inserida na posição
     */
    public void setLine(Integer line) {
        this.line = line;
    }

    /**
     * 
     * Método utilizado para retornar a coluna da posição
     * 
     * @return Integer - Coluna da posição 
     */
    public Integer getColumn() {
        return this.column;
    }

    /**
     * 
     * Método utilizado para incluir uma coluna na posição
     * 
     * @param column Linha a ser inserida na posição
     */
    public void setColumn(Integer column) {
        this.column = column;
    }

    /**
     * 
     * Método utilizado para comparar a posição atual com outra posição
     * 
     * @param position Posição a ser comparada
     * @return int - Caso o valor retornado seja "0" então a posição atual e a posição avaliada são iguais.
     * Caso contrário a posição atual é diferente da posição avaliada
     */
    
    public int compareTo(Position position) {
        if((this.line.compareTo(position.getLine()) == 0) && (this.column.compareTo(position.getColumn()) == 0)) {
            return 0;
        }
        
        return -1;
    }

}
