/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.util;

import graphic.Board;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author Marcio
 */
public class SaveGame {

    private String pathxml = "save.xml";
    private String pathjson = "save.json";
    

    public void saveGameInXML(Board board) {

        try {
            FileWriter arq = new FileWriter(this.pathxml);
            PrintWriter gravarArq = new PrintWriter(arq);

            gravarArq.printf("<?xml version=\"1.0\"?>\n");
            gravarArq.printf("<Board>\n");

            for (int i = 0; i < board.dimension.getLine(); i++) {
                for (int n = 0; n < board.dimension.getColumn(); n++) {
                    gravarArq.printf("<Space line=\"");
                    gravarArq.printf(Integer.toString(i));
                    gravarArq.printf("\" column=\"");
                    gravarArq.printf(Integer.toString(n));
                    gravarArq.printf("\" value=\"");
                    gravarArq.printf(String.valueOf(board.grid[i][n]));
                    gravarArq.printf("\" />\n");
                }
            }

            gravarArq.printf("</Board>");
            arq.close();

        } catch (Exception e) {
        }
    }

    public void loadGameByXML(Board board) {

        int line, column;
        char value;

        try {
            BufferedReader br = new BufferedReader(new FileReader(this.pathxml));
            String str = null;
            while (br.ready()) {
                str = br.readLine();
                if (str.substring(0, 7).equals("<Space ")) {
                    if (str.substring(7, 13).equals("line=\"")) {
                        if (str.substring(16, 24).equals("column=\"")) {
                            if (str.substring(27, 34).equals("value=\"")) {
                                line = Integer.parseInt(str.substring(13, 14));
                                column = Integer.parseInt(str.substring(24, 25));
                                value = str.charAt(34);
                                board.setPosition(value, line, column);
                                //System.out.println("linha " + line + ", coluna " + column + ", value " + (String) value);
                            }
                        }
                    }
                }
            }

            br.close();
        } catch (Exception e) {
        }
    }

    public void saveGameInJSON(Board board) {
        
        try {
            FileWriter arq = new FileWriter(this.pathjson);
            PrintWriter gravarArq = new PrintWriter(arq);
            
            gravarArq.printf("{\n");
            gravarArq.printf("\"board\" : [\n");
            for (int i = 0; i < board.dimension.getLine(); i++) {
                for (int n = 0; n < board.dimension.getColumn(); n++) {
                    gravarArq.printf("{");
                    gravarArq.printf("Line: ");
                    gravarArq.printf("\"");
                    gravarArq.printf(Integer.toString(i));
                    gravarArq.printf("\", ");
                    gravarArq.printf("Column: ");
                    gravarArq.printf("\"");
                    gravarArq.printf(Integer.toString(n));
                    gravarArq.printf("\", ");
                    gravarArq.printf("Value: ");
                    gravarArq.printf("\"");
                    gravarArq.printf(String.valueOf(board.grid[i][n]));
                    gravarArq.printf("\"");
                    if(board.dimension.getLine() == 9 && board.dimension.getLine() == 9){
                        gravarArq.printf("}\n");
                    }else{
                        gravarArq.printf("},\n");
                    }
                }
            }
            gravarArq.printf("]");
            gravarArq.printf("\n}");
            
            arq.close();
        } catch (Exception e) {
        }
        
    }

}