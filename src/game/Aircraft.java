package game;

/**
 *
 * O objetivo desta classe é representar um navio "Aircraft"
 * 
 * @author rosemberg
 */
public class Aircraft extends Ship {

    /**
     * 
     * Construtor de classe que cria um Aircraft
     * 
     */
    public Aircraft() {
        super('A', "Aircraft", 5, 0);
    }
}
