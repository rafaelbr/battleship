package game;

/**
 *
 * O objetivo desta classe é representar um navio "Submarine"
 * 
 * @author rosemberg
 */
public class Submarine extends Ship {

    /**
     * 
     * Construtor de classe que cria um Submarine
     * 
     */
    public Submarine() {
        super('S', "Submarine", 3, 0);
    }
}
