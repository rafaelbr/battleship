package game;

/**
 *
 * O objetivo desta classe é representar um navio "Destroyer"
 * 
 * @author rosemberg
 */
public class Destroyer extends Ship {

    /**
     * 
     * Construtor de classe que cria um Destroyer
     * 
     */
    public Destroyer() {
        super('D', "Destroyer", 2, 0);
    }
}
