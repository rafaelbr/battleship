package game;

import game.util.Position;
import java.util.ArrayList;

/**
 *
 * O objetivo desta classe é representar de forma abstrata um navio
 * 
 * @author rosemberg
 */
public abstract class Ship {

    private char name = '\0';
    private String fullname = "";
    private Integer horizontalSize = 0;
    private Integer verticalSize = 0;
    private Integer direction = -1;
    private ArrayList<Position> position = new ArrayList<>();
    private Integer damage = 0;

    /**
     * 
     * Contrutor de classe para criar um navio
     * 
     * @param name Letra que representa o navio
     * @param fullname Nome completo do navio
     * @param horizontalSize Tamanho horizontal do navio
     * @param verticalSize Tamanho vertical do navio
     * @param direction Direção do navio (0 - vertical; 1 - horizontal)
     */    
    public Ship(char name, String fullname, Integer horizontalSize, Integer verticalSize, Integer direction) {
        this.name = name;
        this.fullname = fullname;
        this.horizontalSize = horizontalSize;
        this.verticalSize = verticalSize;
        this.direction = direction;
    }

     /**
     *
     * Contrutor de classe para criar um navio
     * 
     * @param name Letra que representa o navio
     * @param fullname Nome completo do navio
     * @param horizontalSize Tamanho horizontal do navio
     * @param verticalSize Tamanho vertical do navio
     */
    public Ship(char name, String fullname, Integer horizontalSize, Integer verticalSize) {
        this(name, fullname, horizontalSize, verticalSize, -1);
    }

    /**
     * 
     * Método para incluir uma posição(linha x coluna) a ser ocupada pelo navio
     * 
     * @param line Linha na qual o navio irá ocupar
     * @param column Coluna na qual o navio irá ocupar
     */
    public void setPosition(Integer line, Integer column) {
        this.position.add(new Position(line, column));
    }

    /**
     * 
     * Método para recuperar as posições ocupadas pelo navio
     * 
     * @return ArrayList - Array contendo todas as posições ocupadas pelo navio
     */
    public ArrayList<Position> getPosition() {
        return this.position;
    }

    /**
     * 
     * Método para recuperar a letra que representa o navio
     * 
     * @return char - Letra que representa o navio
     */
    public char getName() {
        return this.name;
    }

    /**
     * 
     * Método para incluir uma letra que represente o navio
     * 
     * @param name Letra a ser atribuída ao navio 
     */
    public void setName(char name) {
        this.name = name;
    }

    /**
     * 
     * Método para recuperar o nome do navio
     * 
     * @return String - String com o nome do navio
     */    
    public String getFullname() {
        return fullname;
    }

    /**
     * 
     * Método para incluir um nome para o navio
     * 
     * @param fullname Nome a ser atribuído ao navio 
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    /**
     * 
     * Método para recuperar o tamanho horizontal do navio
     * 
     * @return Integer - Inteiro que representa o tamanho horizontal do navio
     */
    public Integer getHorizontalSize() {
        return this.horizontalSize;
    }

    /**
     * 
     * Método para incluir o tamanho horizontal do navio
     * 
     * @param horizontalSize Inteiro que representa o tamanho horizontal do navio
     */
    public void setHorizontalSize(Integer horizontalSize) {
        this.horizontalSize = horizontalSize;
    }

     /**
     * 
     * Método para recuperar o tamanho vertical do navio
     * 
     * @return Integer - Inteiro que representa o tamanho vertical do navio
     */
    public Integer getVerticalSize() {
        return this.verticalSize;
    }

    /**
     * 
     * Método para incluir o tamanho horizontal do navio
     * 
     * @param verticalSize Inteiro que representa o tamanho vertical do navio
     */
    public void setVerticalSize(Integer verticalSize) {
        this.verticalSize = verticalSize;
    }

     /**
     * 
     * Método para recuperar a direção do navio (0 - vertical; 1 - horizontal)
     * 
     * @return Integer - Inteiro que representa a direção do navio
     */
    public Integer getDirection() {
        return this.direction;
    }

    /**
     * 
     * Método para incluir a direção do navio (0 - vertical; 1 - horizontal)
     * 
     * @param direction Inteiro que representa a direção do navio
     */
    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    /**
     * 
     * Método para recuperar o dano sofrido pelo navio
     * 
     * @return Integer - Inteiro que representa o dano sofrido pelo navio
     */
    public Integer getDamage() {
        return damage;
    }

    /**
     * 
     * Método para incluir o dano sofrido pelo navio
     * 
     * @param damage Inteiro que representa o dano sofrido pelo navio
     */
    public void setDamage(Integer damage) {
        this.damage = damage;
    }
    
    /**
     * 
     * Método para incrementar o dano sofrido pelo navio
     * 
     * @param damage Inteiro que representa o dano a ser incrementado
     */
    public void increaseDamage(Integer damage) {
        this.damage += damage;
    }

    /**
     * Método para transpor os tamanhos (horizontal <=> vertical) caso haja mudança de direção do navio.
     */
    public void transpose() {
        Integer auxiliar = this.horizontalSize;
        this.horizontalSize = this.verticalSize;
        this.verticalSize = auxiliar;
    }

    /**
     * 
     * Método para verificar se o navio foi atingido na posição de ataque (linha x coluna)
     * 
     * @param line Linha da posição na qual sofreu ataque
     * @param column Coluna da posição na qual sofreu ataque
     * @return boolean Caso o valor retornado seja "true" então o navio foi atingido
     */
    public boolean isReached(Integer line, Integer column) {
        boolean reached = false;
        Position position = null;

        for (int i = 0; i < this.position.size(); ++i) {
            position = this.position.get(i);
            
            if(position.compareTo(new Position(line, column)) == 0) {
                reached = true;
                break;
            }
        }

        return reached;
    }
    
    /**
     * 
     * Método para recuperar o tamanho do navio.
     * 
     * @return Integer - Inteiro que representa o tamanho do navio em 1 dimensão (horizontal ou vertical) 
     */
    public Integer getSize() {
        if(this.horizontalSize > 0) {
            return this.horizontalSize;
        }
        
        return this.verticalSize;
    }
    
    /**
     * 
     * Método utilizado para verificar se o navio foi destruído
     * 
     * @return boolean Caso o valor retornado seja "true" então o navio foi destruído
     */
    public boolean isDestroyed() {
        return (this.getDamage() == this.getSize());
    }

}
